package id.co.downsyndrome.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

/**
 * Created by MGS on 10/18/2017.
 */
public class MessageUtil {

    private static final int MESSAGE_ALERT = 1;
    private static final int CONFIRM_ALERT = 2;
    private static final int DECISION_ALERT = 3;

    public static void showMessage(Context ctx, String message) {
        Toast t = Toast.makeText(ctx, message, Toast.LENGTH_SHORT);
        t.show();
    }

    public static void messageAlert(Context ctx, String title, String message) {
        showAlertDialog(MESSAGE_ALERT, ctx, title, message, null, "OK");
    }

    public static void confirmationAlert(Context ctx, String title, String message, DialogInterface.OnClickListener callBack) {
        showAlertDialog(CONFIRM_ALERT, ctx, title, message, callBack, "Ya","Batal");
    }
    public static void confirmationDelete(Context ctx, DialogInterface.OnClickListener callBack) {
        showAlertDialog(CONFIRM_ALERT, ctx, "Konfirmasi", "Apakah Anda yakin akan menghapus data ini?", callBack, "Ya","Batal");
    }

    public static void decisionAlert(Context ctx, String title, String message, DialogInterface.OnClickListener posCallback, String... buttonNames) {
        showAlertDialog(DECISION_ALERT, ctx, title, message, posCallback, buttonNames);
    }

    public static void showAlertDialog(int alertType, Context ctx, String title, String message, DialogInterface.OnClickListener posCallback, String... buttonNames) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_alert);

        switch (alertType) {
            case MESSAGE_ALERT:
                break;

            case CONFIRM_ALERT:
                builder.setPositiveButton(buttonNames[0], posCallback);
                break;

            case DECISION_ALERT:
                break;
        }

        builder.setNegativeButton(buttonNames [buttonNames.length - 1], new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }
        ).create().show();
    }
}


/**
  sample use
 public class MyActivity extends Activity
 {
 //Called when the activity is first created.
 //Called when the activity is first created.
@Override
public void onCreate(Bundle savedInstanceState)
{
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);

    AlertUtil.messageAlert(this, "title", "message");

    AlertUtil.confirmationAlert(this, "title", "message", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            // do something important, user confirmed the alert
        }
    });

    AlertUtil.decisionAlert(this, "title", "message", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialogInterface, int i) {
            // do something because user clicked the positive button
        }
    });
}
}

 */