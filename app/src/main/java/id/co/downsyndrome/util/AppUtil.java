package id.co.downsyndrome.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by MGS on 11/3/2017.
 */

public class AppUtil {

    public static String getCurentDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date now = new Date();
        return sdfDate.format(now);
    }

    public static String md5(String text) {
        MessageDigest m = null;
        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        m.update(text.getBytes(), 0, text.length());
        String md5 = String.valueOf(new BigInteger(1, m.digest()).toString(16));
        return  md5;
    }

    public static String quote(String txt) {
        return "'" + txt + "'";
    }
}
