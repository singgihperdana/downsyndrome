package id.co.downsyndrome.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import id.co.downsyndrome.model.User;

/**
 * Created by MGS on 10/31/2017.
 */

public class SessionUtil {


    public static User getUserLoggin(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstants.KEY_REFERENCES, Context.MODE_PRIVATE);

        User user = new User();
        user.setIdUser( preferences.getInt(AppConstants.KEY_USER_ID_USERNAME, 0));
        user.setTipe(preferences.getInt(AppConstants.KEY_USER_USERTIPE, -1));
        user.setUserName(preferences.getString(AppConstants.KEY_USER_USERNAME, ""));
        user.setNama(preferences.getString(AppConstants.KEY_USER_NAME, "Guest"));
        user.setPassword(preferences.getString(AppConstants.KEY_USER_PASSWORD, ""));
        Log.i("preferencessss: ", preferences+"");
        Log.i("user.getNama(): ", user.getNama()+"pppp");
        return user;
    }


    public static void createSession(Context context, User user) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(AppConstants.KEY_REFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(AppConstants.KEY_USER_ID_USERNAME, user.getIdUser());
        editor.putString(AppConstants.KEY_USER_USERNAME, user.getUserName());
        editor.putString(AppConstants.KEY_USER_NAME, user.getNama());
        editor.putString(AppConstants.KEY_USER_PASSWORD, user.getPassword());
        editor.putInt(AppConstants.KEY_USER_USERTIPE, user.getTipe());
        editor.commit();
    }

    public static void clearSession(Context context) {
//        SharedPreferences preferences = context.getSharedPreferences(AppConstants.KEY_REFERENCES, Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = preferences.edit();
//        editor.clear();
//        editor.commit();
        Log.i("clearSession", "clearSession");
        context.getSharedPreferences(AppConstants.KEY_REFERENCES, Context.MODE_PRIVATE).edit().clear().commit();
    }
}
