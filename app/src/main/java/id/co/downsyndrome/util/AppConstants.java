package id.co.downsyndrome.util;

/**
 * Created by MGS on 10/19/2017.
 */
public class AppConstants {

    public static String KEY_REFERENCES = "KEY_REFERENCES";
    public static String KEY_USER_ID_USERNAME = "KEY_USER_ID_USERNAME";
    public static String KEY_USER_USERNAME = "KEY_USER_USERNAME";
    public static String KEY_USER_NAME = "KEY_USER_NAME";
    public static String KEY_USER_PASSWORD = "KEY_USER_PASSWORD";
    public static String KEY_USER_USERTIPE = "KEY_USER_USERTIPE";

}
