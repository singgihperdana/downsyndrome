package id.co.downsyndrome.util;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by MGS on 11/3/2017.
 */

public class App extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }

}
