package id.co.downsyndrome.model;

/**
 * Created by MGS on 10/19/2017.
 */
public class User {
    private int idUser;
    private String userName, nama, password, rePassword;
    private int tipe;

    public User() {
    }


    public User(String userName, String nama, String password, String rePassword) {
        this.userName = userName;
        this.nama = nama;
        this.password = password;
        this.rePassword = rePassword;
    }

    public User(int idUser, String userName, String nama, String password, int tipe) {
        this.idUser = idUser;
        this.userName = userName;
        this.nama = nama;
        this.password = password;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public int getTipe() {
        return tipe;
    }

    public void setTipe(int tipe) {
        this.tipe = tipe;
    }
}
