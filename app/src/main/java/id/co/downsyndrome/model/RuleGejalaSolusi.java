package id.co.downsyndrome.model;

/**
 * Created by MGS on 10/30/2017.
 */

public class RuleGejalaSolusi {
    private boolean checked;
    private int id, idGolongan, idKategori;
    private String golonganStr, kategoriStr, keterangan;

    public RuleGejalaSolusi(int id, int idGolongan, int idKategori, String keterangan, String golonganStr, String kategoriStr, boolean checked) {
        this.id = id;
        this.idGolongan = idGolongan;
        this.idKategori = idKategori;
        this.golonganStr = golonganStr;
        this.kategoriStr = kategoriStr;
        this.keterangan = keterangan;
        this.checked = checked;
    }

    public RuleGejalaSolusi() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdGolongan() {
        return idGolongan;
    }

    public void setIdGolongan(int idGolongan) {
        this.idGolongan = idGolongan;
    }

    public int getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(int idKategori) {
        this.idKategori = idKategori;
    }

    public String getGolonganStr() {
        return golonganStr;
    }

    public void setGolonganStr(String golonganStr) {
        this.golonganStr = golonganStr;
    }

    public String getKategoriStr() {
        return kategoriStr;
    }

    public void setKategoriStr(String kategoriStr) {
        this.kategoriStr = kategoriStr;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
