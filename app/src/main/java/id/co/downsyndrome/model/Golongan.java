package id.co.downsyndrome.model;

/**
 * Created by MGS on 10/19/2017.
 */
public class Golongan {
    private int idGolongan;
    private String golongan;

    public Golongan() {
    }

    public int getIdGolongan() {
        return idGolongan;
    }

    public void setIdGolongan(int idGolongan) {
        this.idGolongan = idGolongan;
    }

    public Golongan(int idGolongan, String golongan) {
        this.idGolongan = idGolongan;
        this.golongan = golongan;
    }

    public String getGolongan() {
        return golongan;
    }

    public void setGolongan(String golongan) {
        this.golongan = golongan;
    }
}
