package id.co.downsyndrome.model;

public class Solusi {
    private int idSolusi;
    private String keterangan;
    private boolean checked = false;


    public Solusi(){}

    public Solusi(int idSolusi, String keterangan, boolean checked) {
        this.idSolusi = idSolusi;
        this.keterangan = keterangan;
        this.checked = checked;
    }

    public Solusi(int idSolusi, String keterangan){
        this.idSolusi = idSolusi;
        this.keterangan = keterangan;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public int getIdSolusi() {
        return idSolusi;
    }

    public void setIdSolusi(int idSolusi) {
        this.idSolusi = idSolusi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
