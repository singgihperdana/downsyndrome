package id.co.downsyndrome.model;

/**
 * Created by MGS on 10/17/2017.
 */
public class Kategori {
    private int idKategori;
    private String kode, keterangan;

    public Kategori() {

    }

    public Kategori(int idKategori, String kode, String keterangan) {
        this.idKategori = idKategori;
        this.kode = kode;
        this.keterangan = keterangan;
    }

    public int getIdKategori() {
        return idKategori;
    }

    public void setIdKategori(int idKategori) {
        this.idKategori = idKategori;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
