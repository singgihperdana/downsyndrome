package id.co.downsyndrome;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import id.co.downsyndrome.act.GolonganAct;
import id.co.downsyndrome.act.HomeAct;
import id.co.downsyndrome.act.KategoriAct;
import id.co.downsyndrome.act.KonsultasiAct;
import id.co.downsyndrome.act.SolusiAct;
import id.co.downsyndrome.act.UserAct;
import id.co.downsyndrome.act.RuleGejalaAct;
import id.co.downsyndrome.model.User;
import id.co.downsyndrome.util.SessionUtil;

public class ActMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private FragmentManager fragmentManager;
    private boolean loggedIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //cek session
        User user = SessionUtil.getUserLoggin(getApplicationContext());
        loggedIn = user.getIdUser() == 0 ? false : true;
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //set text tag
        View headerView = navigationView.getHeaderView(0);
        TextView tvUsername = (TextView) headerView.findViewById(R.id.usernameSession);
        tvUsername.setText(user.getNama());
        constructMenuItem();
        callFragment(new HomeAct());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.act_main, menu);
        int group1 = 1;
        int order = 0;
        if (loggedIn) {
           menu.add(group1, 2, order++, "Logout");
        } else {
           menu.add(group1, 1, order++, "Login");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Log.i("getItemId: ", id + "");
        if (id == 1 || id == 2) {
            SessionUtil.clearSession(getApplicationContext());
            redirectLogin();
        }
        return super.onOptionsItemSelected(item);
//        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_home) {
            callFragment(new HomeAct());
        } else if (id == R.id.nav_kategori) {
            callFragment(new KategoriAct());
        } else if (id == R.id.nav_user) {
            callFragment(new UserAct());
        } else if (id == R.id.nav_golongan) {
            callFragment(new GolonganAct());
        } else if (id == R.id.nav_gejala) {
            callFragment(new RuleGejalaAct());
        } else if (id == R.id.nav_solusi) {
            callFragment(new SolusiAct());
        } else if (id == R.id.nav_konsultasi) {
            callFragment(new KonsultasiAct());
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void callFragment(Fragment fragment) {
        fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();
    }

    private void constructMenuItem() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu nav_Menu = navigationView.getMenu();
        if (loggedIn) {
            nav_Menu.findItem(R.id.nav_konsultasi).setVisible(false);
            nav_Menu.findItem(R.id.nav_user).setVisible(true);
            nav_Menu.findItem(R.id.nav_golongan).setVisible(true);
            nav_Menu.findItem(R.id.nav_kategori).setVisible(true);
            nav_Menu.findItem(R.id.nav_gejala).setVisible(true);
            nav_Menu.findItem(R.id.nav_solusi).setVisible(true);
        } else {
            nav_Menu.findItem(R.id.nav_konsultasi).setVisible(true);
            nav_Menu.findItem(R.id.nav_user).setVisible(false);
            nav_Menu.findItem(R.id.nav_golongan).setVisible(false);
            nav_Menu.findItem(R.id.nav_kategori).setVisible(false);
            nav_Menu.findItem(R.id.nav_gejala).setVisible(false);
            nav_Menu.findItem(R.id.nav_solusi).setVisible(false);
        }
    }


    private void redirectLogin() {
        SessionUtil.clearSession(getApplicationContext());
        Intent loginPage = new Intent(this, ActLogin.class);
        startActivity(loginPage);
    }

//    private void redirectHome() {
//        SessionUtil.clearSession(getApplicationContext());
//        Intent homePage = new Intent(this, HomeAct.class);
//        startActivity(homePage);
//    }

    private void exit() {
        new AlertDialog.Builder(ActMain.this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah anda yakin akan keluar?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.finishAffinity(ActMain.this);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


}
