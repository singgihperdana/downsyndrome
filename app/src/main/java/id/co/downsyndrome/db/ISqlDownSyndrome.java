package id.co.downsyndrome.db;

import android.content.Context;

import java.util.List;

import id.co.downsyndrome.model.Golongan;
import id.co.downsyndrome.model.Kategori;
import id.co.downsyndrome.model.Solusi;
import id.co.downsyndrome.model.RuleGejalaSolusi;
import id.co.downsyndrome.model.User;


/**
 * Created by MGS on 10/19/2017.
 */
public interface ISqlDownSyndrome {
    public User login(Context context, String userName, String password);
//
    public List<Kategori> getKategori(Context context);
    public int saveNewKategori(Kategori obj, Context context);
    public void saveUpdateKategori(Kategori obj, Context context);
//    public void deleteKategori(int idMasterKategori, Context context);
    public boolean existKodeKategori(String kode, Context context);

    //user
    public List<User> getUser(Context context);
    public int saveNewUser(User obj, Context context);
    public void saveUpdateUser(User obj, Context context);
    public void deleteUser(int idUser, Context context);
    public boolean existUser(String username, Context context);

    //Golongan
    public List<Golongan> getGolongan(Context context);
    public int saveNewGolongan(Golongan obj, Context context);
    public void saveUpdateGolongan(Golongan obj, Context context);
    public void deleteGolongan(int idGolongan, Context context);
    public boolean existGolongan(String username, Context context);


    //Gejala
    public List<RuleGejalaSolusi> getGejala(Context context);
    public List<RuleGejalaSolusi> getGejala(Context context, int idGolongan);
    public int saveNewGejala(RuleGejalaSolusi obj, Context context);
    public void saveUpdateGejala(RuleGejalaSolusi obj, Context context);
    public void deleteGejala(int idRuleGejala, Context context);
    public boolean existGejala(int idGolongan, int idKategori, Context context);


    //Master Solusi
    public List<Solusi> getMasterSolusi(Context context);
    public List<Solusi> getMasterSolusiByGolonganKategori(int idGolongan, String grupKategori, Context context);
    public List<Solusi> getMasterSolusiByInIdGejala(int idGejala, Context context);
    public List<Solusi> getMasterSolusiByNotInIdGejala(int idGejala, Context context);
    public int saveNewMasterSolusi(String keterangan, Context context);
    public void saveUpdateMasterSolusi(Solusi obj, Context context);
    public void deleteMasterSolusi(int idMasterSolusi, Context context);
    public boolean usedMasterSolusi(int id, Context context);

    //Solusi Gejala
    public void deleteMasterSolusiGejala(List<Solusi> listIdSolusi, int idRuleGejala, Context context);
    public void addMasterSolusiGejala(List<Solusi> listIdSolusi, int idRuleGejala, Context context);


}
