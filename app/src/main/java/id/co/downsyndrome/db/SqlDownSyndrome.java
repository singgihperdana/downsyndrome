package id.co.downsyndrome.db;

import id.co.downsyndrome.util.AppUtil;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import id.co.downsyndrome.model.Golongan;
import id.co.downsyndrome.model.Kategori;
import id.co.downsyndrome.model.Solusi;
import id.co.downsyndrome.model.RuleGejalaSolusi;
import id.co.downsyndrome.model.User;

/**
 * Created by MGS on 10/19/2017.
 */
public class SqlDownSyndrome implements ISqlDownSyndrome {

    private SQLiteDatabase getConnection(Context context) {
        return new DatabaseOpenHelper(context).getWritableDatabase();
    }

    private int getMaxID(String colNameId, String tabel, SQLiteDatabase db) {
        Cursor cs = db.rawQuery("SELECT max("+colNameId+") FROM " + tabel, null);
        cs.moveToFirst();
        if (!cs.isAfterLast()) {
            return cs.getInt(0);
        }
        cs.close();
        return 0;
    }


    @Override
    public User login(Context context, String userName, String password) {
        User user = null;
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery("select id_user, nama, tipe from user where username = "
                + AppUtil.quote(userName) + " and password = " + AppUtil.quote(AppUtil.md5(password)), null);
        cs.moveToFirst();
        if (!cs.isAfterLast()) {
            user = new User();
            user.setIdUser(cs.getInt(0));
            user.setUserName(userName);
            user.setPassword(password);
            user.setNama(cs.getString(1));
            user.setTipe(cs.getInt(2));
        }
        cs.close();
        db.close();

        return user;
    }

    //kategori
    @Override
    public List<Kategori> getKategori(Context context) {
        SQLiteDatabase db = getConnection(context);
        List<Kategori> list = new ArrayList<>();
        Cursor cs = db.rawQuery("SELECT * FROM kategori", null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            list.add(new Kategori(cs.getInt(0), cs.getString(1), cs.getString(2)));
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public int saveNewKategori(Kategori obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "insert into kategori (kode, keterangan) values ('" + obj.getKode() + "', '" + obj.getKeterangan() + "')";
        db.execSQL(sql);
        int id = getMaxID("id_kategori","kategori", db);
        db.close();
        return id;
    }

    @Override
    public void saveUpdateKategori(Kategori obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "update kategori set kode='" + obj.getKode() + "', keterangan = '" + obj.getKeterangan() + "' where id_kategori = " + obj.getIdKategori();
        db.execSQL(sql);
        db.close();
    }

//    @Override
//    public void deleteKategori(int idMasterKategori, Context context) {
//        SQLiteDatabase db = getConnection(context);
//        String sql = "delete from kategori where id = " + idMasterKategori;
//        db.execSQL(sql);
//        db.close();
//    }

    @Override
    public boolean existKodeKategori(String kode, Context context) {
        boolean exist = false;
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery("SELECT * FROM kategori where kode = '" + kode + "'", null);
        cs.moveToFirst();
        if (!cs.isAfterLast()) {
            exist = true;
        }
        cs.close();
        db.close();

        return exist;
    }
    //end of kategori


    //user
    @Override
    public List<User> getUser(Context context) {
        SQLiteDatabase db = getConnection(context);
        List<User> list = new ArrayList<>();
        Cursor cs = db.rawQuery("SELECT id_user, username, nama, password, tipe FROM user", null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            list.add(new User(cs.getInt(0), cs.getString(1), cs.getString(2), cs.getString(3), cs.getInt(4)));
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public int saveNewUser(User obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "insert into user (username, nama, password, tipe) "
                + " values ('" + obj.getUserName() + "', '" + obj.getNama() + "', '" + AppUtil.md5(obj.getPassword()) + "'," + obj.getTipe() + ")";
        db.execSQL(sql);
        int id = getMaxID("id_user","user", db);
        db.close();
        return id;
    }

    @Override
    public void saveUpdateUser(User obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "update user set username=" + AppUtil.quote(obj.getUserName()) + ", nama=" + AppUtil.quote(obj.getNama()) + ", "
                + "password = " + AppUtil.quote(AppUtil.md5(obj.getPassword())) + ", tipe=" + obj.getTipe() + " where id_user = " + obj.getIdUser();
        db.execSQL(sql);
        db.close();
    }

    @Override
    public void deleteUser(int idUser, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "delete from user where id_user = " + idUser;
        db.execSQL(sql);
        db.close();
    }

    @Override
    public boolean existUser(String username, Context context) {
        boolean exist = false;
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery("SELECT * FROM user where username = " + AppUtil.quote(username), null);
        cs.moveToFirst();
        if (!cs.isAfterLast()) {
            exist = true;
        }
        cs.close();
        db.close();

        return exist;
    }
    //end of user


    //start of golongan

    @Override
    public List<Golongan> getGolongan(Context context) {
        SQLiteDatabase db = getConnection(context);
        List<Golongan> list = new ArrayList<>();
        Cursor cs = db.rawQuery("SELECT * FROM golongan", null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            list.add(new Golongan(cs.getInt(0), cs.getString(1)));
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public int saveNewGolongan(Golongan obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "insert into golongan (golongan) "
                + " values (" + AppUtil.quote(obj.getGolongan()) + ")";
        db.execSQL(sql);
        int id = getMaxID("id_golongan", "golongan", db);
        db.close();
        return id;
    }

    @Override
    public void saveUpdateGolongan(Golongan obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "update golongan set golongan=" + AppUtil.quote(obj.getGolongan()) + " where id_golongan=" + obj.getIdGolongan();
        db.execSQL(sql);
        db.close();
    }

    @Override
    public void deleteGolongan(int idGolongan, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "delete from golongan where id_golongan = " + idGolongan;
        db.execSQL(sql);
        db.close();
    }

    @Override
    public boolean existGolongan(String golongan, Context context) {
        boolean exist = false;
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery("SELECT * FROM golongan where golongan = " + AppUtil.quote(golongan), null);
        cs.moveToFirst();
        if (!cs.isAfterLast()) {
            exist = true;
        }
        cs.close();
        db.close();

        return exist;
    }
    //end of golongan


    //start of gejala
    @Override
    public List<RuleGejalaSolusi> getGejala(Context context) {
        List<RuleGejalaSolusi> list = new ArrayList<>();
        String sql = "select g.*, gol.golongan golonganstr, kat.kode from gejala g, golongan gol, kategori kat " +
                " where g.id_golongan = gol.id_golongan and g.id_kategori = kat.id_kategori";
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery(sql, null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            list.add(
                    new RuleGejalaSolusi(
                            cs.getInt(0), cs.getInt(1), cs.getInt(2),
                            cs.getString(3), cs.getString(4), cs.getString(5), false
                    )
            );
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public List<RuleGejalaSolusi> getGejala(Context context, int idGolongan) {
        List<RuleGejalaSolusi> list = new ArrayList<>();
        String sql = "select g.*, gol.golongan golonganstr, kat.kode from gejala g, golongan gol, kategori kat\n" +
                "where g.id_golongan = gol.id_golongan and g.id_kategori = kat.id_kategori and g.id_golongan = " + idGolongan;
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery(sql, null);
        cs.moveToFirst();
        while (!cs.isAfterLast()) {
            list.add(
                    new RuleGejalaSolusi(
                            cs.getInt(0), cs.getInt(1), cs.getInt(2),
                            cs.getString(3), cs.getString(4), cs.getString(5), false
                    )
            );
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public int saveNewGejala(RuleGejalaSolusi obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "insert into gejala (id_golongan, id_kategori, keterangan) "
                + " values (" + obj.getIdGolongan() + ", " + obj.getIdKategori() + ", " + AppUtil.quote(obj.getKeterangan()) + ")";
        db.execSQL(sql);
        int id = getMaxID("id_gejala", "gejala", db);
        db.close();
        return id;
    }

    @Override
    public void saveUpdateGejala(RuleGejalaSolusi obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "update gejala set keterangan=" + AppUtil.quote(obj.getKeterangan())
                + ", id_golongan=" + obj.getIdGolongan()
                + ", id_kategori=" + obj.getIdKategori()
                + " where id=" + obj.getId();
        db.execSQL(sql);
        db.close();
    }

    @Override
    public void deleteGejala(int idRuleGejala, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "delete from gejala where id = " + idRuleGejala;
        db.execSQL(sql);
        db.close();
    }

    @Override
    public boolean existGejala(int idGolongan, int idKategori, Context context) {
        boolean exist = false;
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery("SELECT * FROM gejala where id_golongan = " + idGolongan + " and id_kategori = " + idKategori, null);
        cs.moveToFirst();
        if (!cs.isAfterLast()) {
            exist = true;
        }
        cs.close();
        db.close();

        return exist;
    }
    //end of gejala



    //start of mastersolusi
    @Override
    public List<Solusi> getMasterSolusi(Context context) {
        List<Solusi> list = new ArrayList<>();
        String sql = "select * from solusi s";
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery(sql, null);
        cs.moveToFirst();

        while (!cs.isAfterLast()) {
            list.add(new Solusi(
                    cs.getInt(0), cs.getString(1)
            ));
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public List<Solusi> getMasterSolusiByGolonganKategori(int idGolongan, String grupKategori, Context context) {
        List<Solusi> list = new ArrayList<>();
        SQLiteDatabase db = getConnection(context);
        String sql = "select * from solusi where id_solusi in (select id_solusi from rule_gejala_solusi where id_gejala in (select id_gejala from gejala where id_golongan = " + idGolongan + " and id_kategori in (select id_kategori from kategori where kode in (" + grupKategori + "))))";

        Cursor cs = db.rawQuery(sql, null);
        cs.moveToFirst();

        while (!cs.isAfterLast()) {
            list.add(new Solusi(
                    cs.getInt(0), cs.getString(1)
            ));
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public List<Solusi> getMasterSolusiByInIdGejala(int idGejala, Context context) {
        List<Solusi> list = new ArrayList<>();
        String sql = "select * from solusi where id_solusi in(select id_solusi from rule_gejala_solusi where id_gejala = " + idGejala + ")";
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery(sql, null);
        cs.moveToFirst();

        while (!cs.isAfterLast()) {
            list.add(new Solusi(
                    cs.getInt(0), cs.getString(1)
            ));
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public List<Solusi> getMasterSolusiByNotInIdGejala(int idGejala, Context context) {
        List<Solusi> list = new ArrayList<>();
        String sql = "select * from solusi where id_solusi not in (select id_solusi from rule_gejala_solusi where id_gejala =" + idGejala + ") ";
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery(sql, null);
        cs.moveToFirst();

        while (!cs.isAfterLast()) {
            list.add(new Solusi(
                    cs.getInt(0), cs.getString(1)
            ));
            cs.moveToNext();
        }
        cs.close();
        db.close();
        return list;
    }

    @Override
    public int saveNewMasterSolusi(String keterangan, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "insert into solusi (keterangan) "
                + " values (" + AppUtil.quote(keterangan) + ")";
        db.execSQL(sql);
        int id = getMaxID("id_solusi", "solusi", db);
        db.close();
        return id;
    }

    @Override
    public void saveUpdateMasterSolusi(Solusi obj, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "update solusi set "
                + " keterangan=" + AppUtil.quote(obj.getKeterangan())
                + " where id_solusi=" + obj.getIdSolusi();
        db.execSQL(sql);
        db.close();
    }

    @Override
    public void deleteMasterSolusi(int idMasterSolusi, Context context) {
        SQLiteDatabase db = getConnection(context);
        String sql = "delete from solusi where id_solusi = " + idMasterSolusi;
        db.execSQL(sql);
        db.close();
    }

    @Override
    public boolean usedMasterSolusi(int idMasterSolusi, Context context) {
        // 1345
        String sql = "select count(id_solusi) id_solusi  from rule_gejala_solusi where id_solusi =" + idMasterSolusi;
        SQLiteDatabase db = getConnection(context);
        Cursor cs = db.rawQuery(sql, null);
        cs.moveToFirst();
        int jml = 0;
        if (!cs.isAfterLast()) {
            jml = cs.getInt(0);
        }
        cs.close();
        db.close();
        return jml > 0;
    }

    //Solusi Gejala
    @Override
    public void deleteMasterSolusiGejala(List<Solusi> listSolusi, int idRuleGejala, Context context) {
        SQLiteDatabase db = getConnection(context);
        for (Solusi obj : listSolusi) {
            if (obj.isChecked()) {
                db.execSQL("delete from rule_gejala_solusi where id_gejala =" + idRuleGejala + " and id_solusi = " + obj.getIdSolusi());
            }
        }
        db.close();
    }

    @Override
    public void addMasterSolusiGejala(List<Solusi> listSolusi, int idRuleGejala, Context context) {
        SQLiteDatabase db = getConnection(context);
        for (Solusi obj : listSolusi) {
            if (obj.isChecked()) {
                db.execSQL("insert into rule_gejala_solusi (id_gejala, id_solusi)"
                        + " values (" + idRuleGejala + "," + obj.getIdSolusi() + ")");
            }
        }
        db.close();
    }
    //end of Solusi Gejala

}
