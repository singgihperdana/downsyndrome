package id.co.downsyndrome.act;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.adapter.GejalaCBoxAdapter;
import id.co.downsyndrome.adapter.GolonganSpinnerAdapter;
import id.co.downsyndrome.adapter.SolusiAdapter;
import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.Golongan;
import id.co.downsyndrome.model.Solusi;
import id.co.downsyndrome.model.RuleGejalaSolusi;
import id.co.downsyndrome.util.AppUtil;
import id.co.downsyndrome.util.MessageUtil;

/**
 * Created by MGS on 10/17/2017.
 */
public class KonsultasiAct extends Fragment {


    private ListView listViewGejala;
    private List<String> listGejala;
    private RelativeLayout view;
    private Spinner spinnerGolongan;
    private List<RuleGejalaSolusi> listRuleGejalaSolusi;

    //dialog
    private ListView listViewGejalaDialog;
    private ArrayAdapter listViewGejalaAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = (RelativeLayout) inflater.inflate(R.layout.konsultasi_view, container, false);
        getActivity().setTitle("Konsultasi");
        initSpinner();
        initListViewGejala();
        initBtnAddGejala();
        initBtnViewSolusi();
        return view;
    }


    private void initSpinner() {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        spinnerGolongan = (Spinner) view.findViewById(R.id.inputKonsulGolongan);
        List<Golongan> listGolongan = sql.getGolongan(view.getContext());
        ArrayAdapter<Golongan> spinGolonganAdapter = new GolonganSpinnerAdapter(view.getContext(), android.R.layout.simple_spinner_item, listGolongan.toArray(new Golongan[listGolongan.size()]));
        spinGolonganAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGolongan.setAdapter(spinGolonganAdapter);
    }

    private void initListViewGejala() {
        listViewGejala = view.findViewById(R.id.listViewKonsulRuleGejala);
        listGejala = new ArrayList<>();
        listViewGejalaAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.master_solusi_list_grupkat_text_view, listGejala);
        listViewGejala.setAdapter(listViewGejalaAdapter);
        listViewGejala.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                final String str = (String) listViewGejala.getItemAtPosition(position);
                initContextViewGrupKategori(str);
                return false;
            }
        });
    }

    private void initContextViewGrupKategori(final String str) {
        final CharSequence[] itemMenus = {"Delete"};
        final Context ctx = view.getContext();

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setItems(itemMenus, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                listGejala.remove(str);
                listViewGejalaAdapter.notifyDataSetChanged();
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    private void initBtnAddGejala() {
        Button btn = (Button) view.findViewById(R.id.btnAddGejala);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogGejalaCheckBox();
            }
        });
    }

    private void initBtnViewSolusi() {
        Button btn = (Button) view.findViewById(R.id.btnViewSolusi);
        final ISqlDownSyndrome sql = new SqlDownSyndrome();
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Golongan selectedGolongan = (Golongan) spinnerGolongan.getSelectedItem();
                if (listGejala.isEmpty()) {
                    MessageUtil.showMessage(view.getContext(), "Gejala belum dipilih");
                } else {
                    String grupKat = "";
                    for (RuleGejalaSolusi ruleGejalaSolusi : listRuleGejalaSolusi) {
                        grupKat += AppUtil.quote(ruleGejalaSolusi.getKategoriStr());
                        grupKat += ",";
                    }
                    grupKat = grupKat.substring(0, grupKat.length() - 1);
                    List<Solusi> listSolusi = sql.getMasterSolusiByGolonganKategori(selectedGolongan.getIdGolongan(), grupKat, view.getContext());
                    if (listSolusi.isEmpty()) {
                        MessageUtil.showMessage(view.getContext(), "Solusi tidak ditemukan");
                    } else {
                        showDialogSolusi(view.getContext(), listSolusi);
                    }
                }
            }
        });
    }

    private void showDialogSolusi(final Context ctx, final List<Solusi> listSolusi) {
        // LayoutInflater inflater = (LayoutInflater) ctx.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        alertDialogBuilderEdit.setTitle("Lihat Solusi");
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        final View dialogSolusiView = inflater.inflate(R.layout.gejala_view_dialog_solusi, null);
        alertDialogBuilderEdit.setView(dialogSolusiView);

        SolusiAdapter solusiAdapter = new SolusiAdapter(view.getContext(), R.layout.mastersolusi_view_list_adapter, listSolusi);

        ListView listViewMasterSolusi = (ListView) dialogSolusiView.findViewById(R.id.gejalaDialogListSolusi);
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.mastersolusi_view_list_header, listViewMasterSolusi, false);
        listViewMasterSolusi.addHeaderView(header);
        listViewMasterSolusi.setAdapter(solusiAdapter);

//        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan Konsultasi", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                //Do nothing here because we override this button later to change the close behaviour.
//            }
//        });
        alertDialogBuilderEdit.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
    }


    private void showDialogGejalaCheckBox() {
        final ISqlDownSyndrome sql = new SqlDownSyndrome();
        final AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(view.getContext());
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        final View dialogSolusiView = inflater.inflate(R.layout.gejala_view_dialog_solusi, null);
        alertDialogBuilderEdit.setView(dialogSolusiView);

        alertDialogBuilderEdit.setTitle("Pilih Gejala");
        Golongan gol = (Golongan) spinnerGolongan.getSelectedItem();
        listRuleGejalaSolusi = sql.getGejala(view.getContext(), gol.getIdGolongan());

        /**
         * https://stackoverflow.com/questions/13533715/java-util-concurrentmodificationexception-android-after-remove-elements-from-arr
         * You can't remove items from a list while iterating over it. You need to use an iterator and its remove method:
         * */
        for (Iterator<RuleGejalaSolusi> it = listRuleGejalaSolusi.iterator(); it.hasNext(); ) {
            RuleGejalaSolusi obj = it.next();
            for (String gejala : listGejala) {
                if (obj.getKeterangan().equals(gejala)) {
                    it.remove();
                }
            }
        }

        GejalaCBoxAdapter gejalaCBoxAdapter = new GejalaCBoxAdapter(view.getContext(), listRuleGejalaSolusi);
        listViewGejalaDialog = (ListView) dialogSolusiView.findViewById(R.id.gejalaDialogListSolusi);
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.mastersolusi_view_list_cbox_header, listViewGejalaDialog, false);
        listViewGejalaDialog.addHeaderView(header);
        listViewGejalaDialog.setAdapter(gejalaCBoxAdapter);


        // add event listener----When list view item is clicked.
        listViewGejalaDialog.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemIndex, long l) {
                Object itemObject = adapterView.getAdapter().getItem(itemIndex);
                RuleGejalaSolusi obj = (RuleGejalaSolusi) itemObject;
                CheckBox itemCheckbox = (CheckBox) view.findViewById(R.id.gejala_checkbox);
                if (obj.isChecked()) {
                    itemCheckbox.setChecked(false);
                    obj.setChecked(false);
                } else {
                    itemCheckbox.setChecked(true);
                    obj.setChecked(true);
                }
            }
        });

        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        alertDialogBuilderEdit.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean added = false;
                for (RuleGejalaSolusi obj : listRuleGejalaSolusi) {
                    if (obj.isChecked()) {
                        listGejala.add(obj.getKeterangan());
                        added = true;
                    }
                }

                if (added) {
                    listViewGejalaAdapter.notifyDataSetChanged();
                    MessageUtil.showMessage(view.getContext(), "Tambah gejala berhasil");
                }
                dialog.dismiss();
            }
        });
    }
}
