package id.co.downsyndrome.act;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;


import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.adapter.GolonganAdapter;
import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.Golongan;
import id.co.downsyndrome.util.AppUtil;
import id.co.downsyndrome.util.MessageUtil;

public class GolonganAct extends Fragment {

    private List<Golongan> listGolongan;
    private GolonganAdapter arrayAdapter;
    private ListView listView;
    private RelativeLayout view;

    private boolean isNew;

    public GolonganAct() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = (RelativeLayout) inflater.inflate(R.layout.golongan_view, container, false);
        getActivity().setTitle("Golongan");

        listGolongan = new SqlDownSyndrome().getGolongan(view.getContext());
        arrayAdapter = new GolonganAdapter(view.getContext(), R.layout.golongan_view_list_adapter, listGolongan);

        listView = (ListView) view.findViewById(R.id.listMasterGolongan);

        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.golongan_view_list_header, listView, false);
        listView.addHeaderView(header);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                if (position > 0) {
                    final Golongan obj = (Golongan) parent.getItemAtPosition(position);
                    initContextMenuTable(view, obj);
                }
                return false;
            }
        });


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btn_add_golongan);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isNew = true;
                showDialogInput(view.getContext(), new Golongan());
                MessageUtil.showMessage(view.getContext(), "Ok");
            }
        });

        return view;
    }

    //
    private void initContextMenuTable(final View view, final Golongan obj) {
        final ISqlDownSyndrome sql = new SqlDownSyndrome();
        final CharSequence[] itemMenus = {"Delete", "Edit"};
        final Context ctx = view.getContext();
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("Action:");
        alertDialogBuilder.setItems(itemMenus, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    MessageUtil.confirmationDelete(ctx,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    sql.deleteGolongan(obj.getIdGolongan(),ctx);
                                    listGolongan.remove(obj);
                                    arrayAdapter.notifyDataSetChanged();
                                    MessageUtil.showMessage(ctx, "Hapus data berhasil");
                                }
                            }
                    );
                } else {
                    isNew = false;
                    showDialogInput(ctx, obj);
                }
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showDialogInput(final Context ctx, final Golongan obj) {
        AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        if (isNew) {
            alertDialogBuilderEdit.setTitle("Tambah Golongan");
        } else {
            alertDialogBuilderEdit.setTitle("Edit Golongan");
        }
        LayoutInflater li = LayoutInflater.from(view.getContext());
        final View golonganInputView = li.inflate(R.layout.golongan_input, null);
        alertDialogBuilderEdit.setView(golonganInputView);
        final EditText userInputGolongan = (EditText) golonganInputView.findViewById(R.id.inputGolongan);
        if (!isNew) {
            userInputGolongan.setText(obj.getGolongan());
        }

        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing here because we override this button later to change the close behaviour.
            }
        });

        alertDialogBuilderEdit.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });


        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obj.setGolongan(userInputGolongan.getText().toString().trim());
                if (obj.getGolongan().equals("")) {
                    MessageUtil.showMessage(ctx, "Golongan tidak boleh kosong");
                }

                if (isNew) {
                    if (saveNew(ctx, obj)) {
                        dialog.dismiss();
                    }
                } else {
                    if (saveUpdate(ctx, obj)) {
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    private boolean saveNew(Context ctx, Golongan obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        if (sql.existGolongan(obj.getGolongan(), ctx)) {
            MessageUtil.showMessage(ctx, "Kode sudah ada");
            return false;
        } else {
            obj.setIdGolongan(sql.saveNewGolongan(obj, ctx));
            listGolongan.add(obj);
            arrayAdapter.notifyDataSetChanged();
            MessageUtil.showMessage(ctx, "Tambah data berhasil");
            return true;
        }
    }

    private boolean saveUpdate(Context ctx, Golongan obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        for (int i = 0; i < listGolongan.size(); i++) {
            if (listGolongan.get(i).getIdGolongan() != obj.getIdGolongan()) {
                if (listGolongan.get(i).getGolongan().equals(obj.getGolongan())) {
                    MessageUtil.showMessage(ctx, "Golongan sudah ada");
                    return false;
                }
            }
        }

        for (int i = 0; i < listGolongan.size(); i++) {
            if (listGolongan.get(i).getIdGolongan() == obj.getIdGolongan()) {
                sql.saveUpdateGolongan(obj, ctx);
                listGolongan.set(i, obj);
                arrayAdapter.notifyDataSetChanged();
                MessageUtil.showMessage(ctx, "Update data berhasil");
                return true;
            }
        }
        return false;
    }


}
