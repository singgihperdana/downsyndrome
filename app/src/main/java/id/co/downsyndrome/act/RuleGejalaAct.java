package id.co.downsyndrome.act;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.adapter.GolonganSpinnerAdapter;
import id.co.downsyndrome.adapter.KategoriSpinnerAdapter;
import id.co.downsyndrome.adapter.SolusiCBoxAdapter;
import id.co.downsyndrome.adapter.SolusiAdapter;
import id.co.downsyndrome.adapter.RuleGejalaAdapter;
import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.Golongan;
import id.co.downsyndrome.model.Kategori;
import id.co.downsyndrome.model.Solusi;
import id.co.downsyndrome.model.RuleGejalaSolusi;
import id.co.downsyndrome.util.MessageUtil;

/**
 * Created by MGS on 10/17/2017.
 */
public class RuleGejalaAct extends Fragment {


    private List<RuleGejalaSolusi> listRuleGejalaSolusi;
    private RuleGejalaAdapter ruleGejalaAdapter;
    private ListView listView;
    private RelativeLayout view;

    private Spinner spinnerKategori, spinnerGolongan;

    //dialog
    ListView listViewMasterSolusi;
    private List<Solusi> listSolusi;
    private SolusiAdapter solusiAdapter;


    private boolean isNew;

    public RuleGejalaAct() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = (RelativeLayout) inflater.inflate(R.layout.gejala_view, container, false);
        getActivity().setTitle("Rule");

        listRuleGejalaSolusi = new SqlDownSyndrome().getGejala(view.getContext());
        ruleGejalaAdapter = new RuleGejalaAdapter(view.getContext(), R.layout.gejala_view_list_adapter, listRuleGejalaSolusi);

        listView = (ListView) view.findViewById(R.id.listGejalaView);

        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.gejala_view_list_header, listView, false);
        listView.addHeaderView(header);
        listView.setAdapter(ruleGejalaAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                if (position > 0) {
                    final RuleGejalaSolusi obj = (RuleGejalaSolusi) parent.getItemAtPosition(position);
                    initContextMenuTable(view, obj);
                }
                return false;
            }
        });


        //
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btn_add_gejala);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isNew = true;
                showDialogInputRule(view.getContext(), new RuleGejalaSolusi());
                MessageUtil.showMessage(view.getContext(), "Ok");
            }
        });


        return view;
    }


    private void showDialogSolusi(final Context ctx, final int idRuleGejala, int itemAction) {
        // LayoutInflater inflater = (LayoutInflater) ctx.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        alertDialogBuilderEdit.setTitle("Lihat Solusi");
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        final View dialogSolusiView = inflater.inflate(R.layout.gejala_view_dialog_solusi, null);
        alertDialogBuilderEdit.setView(dialogSolusiView);

        listSolusi = new SqlDownSyndrome().getMasterSolusiByInIdGejala(idRuleGejala, view.getContext());
        solusiAdapter = new SolusiAdapter(view.getContext(), R.layout.mastersolusi_view_list_adapter, listSolusi);

        listViewMasterSolusi = (ListView) dialogSolusiView.findViewById(R.id.gejalaDialogListSolusi);
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.mastersolusi_view_list_header, listViewMasterSolusi, false);
        listViewMasterSolusi.addHeaderView(header);
        listViewMasterSolusi.setAdapter(solusiAdapter);

        alertDialogBuilderEdit.setNegativeButton("Tutup", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
    }

    private void showDialogSolusiCheckBox(final Context ctx, final int idRuleGejala, final int itemAction) {
        final ISqlDownSyndrome sql = new SqlDownSyndrome();
        final AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        LayoutInflater inflater = LayoutInflater.from(view.getContext());
        final View dialogSolusiView = inflater.inflate(R.layout.gejala_view_dialog_solusi, null);
        alertDialogBuilderEdit.setView(dialogSolusiView);
        if (itemAction == 1) {
            alertDialogBuilderEdit.setTitle("Tambah Solusi");
            listSolusi = sql.getMasterSolusiByNotInIdGejala(idRuleGejala, ctx);
        } else if (itemAction == 2) {
            alertDialogBuilderEdit.setTitle("Hapus Solusi");
            listSolusi =sql.getMasterSolusiByInIdGejala(idRuleGejala, ctx);
        }
        SolusiCBoxAdapter solusiCBoxAdapter = new SolusiCBoxAdapter(ctx, listSolusi);
        listViewMasterSolusi = (ListView) dialogSolusiView.findViewById(R.id.gejalaDialogListSolusi);
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.mastersolusi_view_list_cbox_header, listViewMasterSolusi, false);
        listViewMasterSolusi.addHeaderView(header);
        listViewMasterSolusi.setAdapter(solusiCBoxAdapter);


        // add event listener----When list view item is clicked.
        listViewMasterSolusi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int itemIndex, long l) {
                Object itemObject = adapterView.getAdapter().getItem(itemIndex);
                Solusi obj = (Solusi) itemObject;
                CheckBox itemCheckbox = (CheckBox) view.findViewById(R.id.master_solusi_list_view_item_checkbox);
                if (obj.isChecked()) {
                    itemCheckbox.setChecked(false);
                    obj.setChecked(false);
                } else {
                    itemCheckbox.setChecked(true);
                    obj.setChecked(true);
                }
            }
        });

        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing here because we override this button later to change the close behaviour.
            }
        });
        alertDialogBuilderEdit.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemAction == 1) {
                    sql.addMasterSolusiGejala(listSolusi, idRuleGejala, ctx);
                    MessageUtil.showMessage(ctx, "Tambah data berhasil");
                } else if (itemAction == 2) {
                    MessageUtil.confirmationDelete(ctx,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    sql.deleteMasterSolusiGejala(listSolusi, idRuleGejala, ctx);
                                    MessageUtil.showMessage(ctx, "Hapus data berhasil");
                                }
                            }
                    );
                }
                dialog.dismiss();
            }
        });
    }

    private void initContextMenuTable(final View view, final RuleGejalaSolusi obj) {
        final CharSequence[] itemMenus = {"Lihat Solusi", "Tambah Solusi", "Hapus Solusi", "Hapus Rule", "Edit Rule"};
        final Context ctx = view.getContext();
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("Action:");
        alertDialogBuilder.setItems(itemMenus, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                Log.i("item: ", String.valueOf(item));
                if (item == 0) {
                    showDialogSolusi(ctx, obj.getId(), item);
                } else if (item == 3) {
                    ISqlDownSyndrome sql = new SqlDownSyndrome();
                    sql.deleteGejala(obj.getId(), ctx);
                    listRuleGejalaSolusi.remove(obj);
                    ruleGejalaAdapter.notifyDataSetChanged();
                } else if (item == 4) {
                    isNew = false;
                    showDialogInputRule(ctx, obj);
                } else {
                    showDialogSolusiCheckBox(ctx, obj.getId(), item);
                }
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showDialogInputRule(final Context ctx, final RuleGejalaSolusi obj) {
        AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        if (isNew) {
            alertDialogBuilderEdit.setTitle("Tambah Gejala");
        } else {
            alertDialogBuilderEdit.setTitle("Edit Gejala");
        }
        LayoutInflater li = LayoutInflater.from(view.getContext());
        final View gejalaInputView = li.inflate(R.layout.gejala_input, null);
        alertDialogBuilderEdit.setView(gejalaInputView);

        //fill spinner golongan
        fillSpinner(gejalaInputView);

        final EditText userInputKet = (EditText) gejalaInputView.findViewById(R.id.inputGejalaKeterangan);


        if (!isNew) {
            userInputKet.setText(obj.getKeterangan());
            for (int i = 0; i < spinnerKategori.getCount(); i++) {
                Kategori tmp = (Kategori) spinnerKategori.getItemAtPosition(i);
                if (tmp.getIdKategori() == obj.getIdKategori()) {
                    spinnerKategori.setSelection(i);
                    break;
                }
            }

            for (int i = 0; i < spinnerGolongan.getCount(); i++) {
                Golongan tmp = (Golongan) spinnerGolongan.getItemAtPosition(i);
                if (tmp.getIdGolongan() == obj.getIdGolongan()) {
                    spinnerGolongan.setSelection(i);
                    break;
                }
            }
        }

        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing here because we override this button later to change the close behaviour.
            }
        });

        alertDialogBuilderEdit.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });


//      alertDialogBuilderEdit.create().show();
        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Golongan selectedGolongan = (Golongan) spinnerGolongan.getSelectedItem();
                Kategori selectedKategori = (Kategori) spinnerKategori.getSelectedItem();

                //MessageUtil.showMessage(v.getContext(),a "" + );
                obj.setKeterangan(userInputKet.getText().toString());
                obj.setGolonganStr(selectedGolongan.getGolongan());
                obj.setIdGolongan(selectedGolongan.getIdGolongan());
                obj.setKategoriStr(selectedKategori.getKode());
                obj.setIdKategori(selectedKategori.getIdKategori());

                if (obj.getKeterangan().equals("")) {
                    MessageUtil.showMessage(ctx, "Keterangan tidak boleh kosong");
                    return;
                }

                if (isNew) {
                    if (saveNewRule(ctx, obj)) {
                        dialog.dismiss();
                    }
                } else {
                    if (saveUpdateRule(ctx, obj)) {
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    private boolean saveNewRule(Context ctx, RuleGejalaSolusi obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        if (sql.existGejala(obj.getIdGolongan(), obj.getIdKategori(), ctx)) {
            MessageUtil.showMessage(ctx, "Rule gejala sudah ada");
            return false;
        }
        obj.setId(sql.saveNewGejala(obj, ctx));
        listRuleGejalaSolusi.add(obj);
        ruleGejalaAdapter.notifyDataSetChanged();
        MessageUtil.showMessage(ctx, "Tambah data berhasil");
        return true;
    }

    private boolean saveUpdateRule(Context ctx, RuleGejalaSolusi obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        for (int i = 0; i < listRuleGejalaSolusi.size(); i++) {
            if (listRuleGejalaSolusi.get(i).getId() == obj.getId()) {
                sql.saveUpdateGejala(obj, ctx);
                listRuleGejalaSolusi.set(i, obj);
                ruleGejalaAdapter.notifyDataSetChanged();
                MessageUtil.showMessage(ctx, "Update data berhasil");
                return true;
            }
        }
        return false;
    }

    private void fillSpinner(View gejalaInputView) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        spinnerGolongan = (Spinner) gejalaInputView.findViewById(R.id.inputGejalaGolongan);
        List<Golongan> listGolongan = sql.getGolongan(view.getContext());
        ArrayAdapter<Golongan> spinGolonganAdapter = new GolonganSpinnerAdapter(gejalaInputView.getContext(), android.R.layout.simple_spinner_item,
                listGolongan.toArray(new Golongan[listGolongan.size()]));
        spinGolonganAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGolongan.setAdapter(spinGolonganAdapter);

        //fill spinner kategori
        spinnerKategori = (Spinner) gejalaInputView.findViewById(R.id.inputGejalaKategori);
        List<Kategori> listKategori = sql.getKategori(view.getContext());
        ArrayAdapter<Kategori> spinKategoriAdapter = new KategoriSpinnerAdapter(gejalaInputView.getContext(),
                android.R.layout.simple_spinner_item,
                listKategori.toArray(new Kategori[listKategori.size()]));
        spinKategoriAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerKategori.setAdapter(spinKategoriAdapter);
    }
}
