package id.co.downsyndrome.act;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.User;

/**
 * Created by MGS on 10/16/2017.
 */
public class HomeAct extends Fragment {
    public HomeAct(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home, container, false);
        getActivity().setTitle("Home");
        return rootView;
    }

}
