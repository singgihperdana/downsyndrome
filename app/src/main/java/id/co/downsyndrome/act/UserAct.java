package id.co.downsyndrome.act;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.adapter.UserAdapter;
import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.User;
import id.co.downsyndrome.util.MessageUtil;
import id.co.downsyndrome.util.SessionUtil;


/**
 * Created by MGS on 10/17/2017.
 */
public class UserAct extends Fragment {


    private List<User> listUser;
    private UserAdapter arrayAdapter;
    private ListView listView;
    private RelativeLayout view;

    private boolean isNew;

    public UserAct() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = (RelativeLayout) inflater.inflate(R.layout.user_view, container, false);
        getActivity().setTitle("Master user");

        listUser = new SqlDownSyndrome().getUser(view.getContext());
        arrayAdapter = new UserAdapter(view.getContext(), R.layout.user_view_list_adapter, listUser);

        listView = (ListView) view.findViewById(R.id.listUserView);

        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.user_view_header, listView,
                false);
        listView.addHeaderView(header);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                if (position > 0) {
                    final User obj = (User) parent.getItemAtPosition(position);
                    initContextMenuTable(view, obj);
                }
                return false;
            }
        });


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btn_add_user);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isNew = true;
                showDialogInput(view.getContext(), new User());
                MessageUtil.showMessage(view.getContext(), "Ok");
            }
        });

        return view;
    }

    private void initContextMenuTable(final View view, final User obj) {
        final CharSequence[] itemMenus = {"Delete", "Edit"};
        final Context ctx = view.getContext();

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("Action:");
        alertDialogBuilder.setItems(itemMenus, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    if (obj.getUserName().equals("admin")) {
                        MessageUtil.showMessage(ctx, "User admin tidak bisa dihapus");
                        return;
                    }

                    MessageUtil.confirmationDelete(ctx,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    new SqlDownSyndrome().deleteUser(obj.getIdUser(), ctx);
                                    listUser.remove(obj);
                                    arrayAdapter.notifyDataSetChanged();
                                    MessageUtil.showMessage(ctx, "Hapus data berhasil");
                                }
                            }
                    );

                } else {
                    isNew = false;
                    showDialogInput(ctx, obj);
                }
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showDialogInput(final Context ctx, final User obj) {
        AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        if (isNew) {
            alertDialogBuilderEdit.setTitle("Tambah User");
        } else {
            alertDialogBuilderEdit.setTitle("Edit User");
        }
        LayoutInflater li = LayoutInflater.from(view.getContext());
        final View userView = li.inflate(R.layout.user_input, null);
        alertDialogBuilderEdit.setView(userView);
        final EditText userInputUsername = (EditText) userView.findViewById(R.id.inputUsernameUser);
        final EditText userInputNama = (EditText) userView.findViewById(R.id.inputNamaUser);
        final EditText userInputPassword1 = (EditText) userView.findViewById(R.id.inputPasswordUser);
        final EditText userInputPassword2 = (EditText) userView.findViewById(R.id.inputRePasswordUser);
        if (!isNew) {
            userInputUsername.setText(obj.getUserName());
            userInputNama.setText(obj.getNama());
            User userLoggin = SessionUtil.getUserLoggin(ctx);
            Log.i("getPassword()", userLoggin.getPassword());
            userInputPassword1.setText(userLoggin.getPassword());
            userInputPassword2.setText(userLoggin.getPassword());
        }

        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing here because we override this button later to change the close behaviour.
            }
        });

        alertDialogBuilderEdit.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obj.setUserName(userInputUsername.getText().toString().trim());
                obj.setNama(userInputNama.getText().toString().trim());
                obj.setPassword(userInputPassword1.getText().toString().trim());
                obj.setRePassword(userInputPassword2.getText().toString().trim());
                if (obj.getUserName().equals("")) {
                    MessageUtil.showMessage(ctx, "Username tidak boleh kosong");
                } else if (obj.getNama().equals("")) {
                    MessageUtil.showMessage(ctx, "Nama tidak boleh kosong");
                } else if (obj.getPassword().equals("") || obj.getRePassword().equals("")) {
                    MessageUtil.showMessage(ctx, "Password tidak boleh kosong");
                } else if (!obj.getPassword().equals(obj.getRePassword())) {
                    MessageUtil.showMessage(ctx, "Password harus sama");
                } else {
                    if (isNew) {
                        if (saveNew(ctx, obj)) {
                            dialog.dismiss();
                        }
                    } else {
                        if (saveUpdate(ctx, obj)) {
                            dialog.dismiss();
                        }
                    }
                }
            }
        });
    }

    private boolean saveNew(Context ctx, User obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        obj.setTipe(1);
        if (sql.existUser(obj.getUserName(), ctx)) {
            MessageUtil.showMessage(ctx, "Username sudah ada");
            return false;
        } else {
            obj.setIdUser(sql.saveNewUser(obj, ctx));
            listUser.add(obj);
            arrayAdapter.notifyDataSetChanged();
            MessageUtil.showMessage(ctx, "Tambah data berhasil");
            return true;
        }
    }

    private boolean saveUpdate(Context ctx, User obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        for (int i = 0; i < listUser.size(); i++) {
            if (listUser.get(i).getIdUser() != obj.getIdUser()) {
                if (listUser.get(i).getUserName().equals(obj.getUserName())) {
                    MessageUtil.showMessage(ctx, "Username sudah ada");
                    return false;
                }
            }
        }

        for (int i = 0; i < listUser.size(); i++) {
            if (listUser.get(i).getIdUser() == obj.getIdUser()) {
                sql.saveUpdateUser(obj, ctx);
                listUser.set(i, obj);
                arrayAdapter.notifyDataSetChanged();
                MessageUtil.showMessage(ctx, "Update data berhasil");
                return true;
            }
        }
        return false;
    }

}
