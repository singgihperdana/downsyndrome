package id.co.downsyndrome.act;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.adapter.KategoriAdapter;
import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.Kategori;
import id.co.downsyndrome.util.MessageUtil;


/**
 * Created by MGS on 10/17/2017.
 */
public class KategoriAct extends Fragment {


    private List<Kategori> listKategori;
    private KategoriAdapter arrayAdapter;
    private ListView listView;
    private RelativeLayout view;

    private boolean isNew;

    public KategoriAct() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = (RelativeLayout) inflater.inflate(R.layout.masterkategori_view, container, false);
        getActivity().setTitle("Kategori");

        listKategori = new SqlDownSyndrome().getKategori(view.getContext());
        arrayAdapter = new KategoriAdapter(view.getContext(), R.layout.masterkategori_view_list_adapter, listKategori);

        listView = (ListView) view.findViewById(R.id.listMasterKategoriView);

        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.masterkategori_view_list_header, listView, false);
        listView.addHeaderView(header);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                if (position > 0) {
                    final Kategori obj = (Kategori) parent.getItemAtPosition(position);
                    initContextMenuTable(view, obj);
                }
                return false;
            }
        });


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btn_add_masterkategori);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isNew = true;
                showDialogInput(view.getContext(), new Kategori());
                MessageUtil.showMessage(view.getContext(), "Ok");
            }
        });

        return view;
    }

    private void initContextMenuTable(final View view, final Kategori obj) {
        final ISqlDownSyndrome sql = new SqlDownSyndrome();
        final CharSequence[] itemMenus = {"Delete", "Edit"};
        final Context ctx = view.getContext();

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("Action:");
        alertDialogBuilder.setItems(itemMenus, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {

                    MessageUtil.confirmationDelete(ctx,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    sql.deleteGolongan(obj.getIdKategori(),ctx);
                                    listKategori.remove(obj);
                                    arrayAdapter.notifyDataSetChanged();
                                    MessageUtil.showMessage(ctx, "Hapus data berhasil");
                                }
                            }
                    );
                } else {
                    isNew = false;
                    showDialogInput(ctx, obj);
                }
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showDialogInput(final Context ctx, final Kategori obj) {
        AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        if (isNew) {
            alertDialogBuilderEdit.setTitle("Tambah Kategori");
        } else {
            alertDialogBuilderEdit.setTitle("Edit Kategori");
        }
        LayoutInflater li = LayoutInflater.from(view.getContext());
        final View masterKategoriInputView = li.inflate(R.layout.masterkategori_input, null);
        alertDialogBuilderEdit.setView(masterKategoriInputView);
        final EditText userInputKode = (EditText) masterKategoriInputView.findViewById(R.id.inputMasterKategoriKode);
        final EditText userInputKet = (EditText) masterKategoriInputView.findViewById(R.id.inputMasterKategoriKet);
        if (!isNew) {
            userInputKode.setText(obj.getKode());
            userInputKet.setText(obj.getKeterangan());
        }

        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing here because we override this button later to change the close behaviour.
            }
        });

        alertDialogBuilderEdit.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });


        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obj.setKode(userInputKode.getText().toString().trim());
                obj.setKeterangan(userInputKet.getText().toString().trim());
                if (obj.getKode().equals("")) {
                    MessageUtil.showMessage(ctx, "Kode tidak boleh kosong");
                } else if (obj.getKeterangan().equals("")) {
                    MessageUtil.showMessage(ctx, "Keterangan tidak boleh kosong");
                }

                if (isNew) {
                    if (saveNew(ctx, obj)) {
                        dialog.dismiss();
                    }
                } else {
                    if (saveUpdate(ctx, obj)) {
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    private boolean saveNew(Context ctx, Kategori obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        if (sql.existKodeKategori(obj.getKode(), ctx)) {
            MessageUtil.showMessage(ctx, "Kode sudah ada");
            return false;
        } else {
            obj.setIdKategori(sql.saveNewKategori(obj, ctx));
            listKategori.add(obj);
            arrayAdapter.notifyDataSetChanged();
            MessageUtil.showMessage(ctx, "Tambah data berhasil");
            return true;
        }
    }

    private boolean saveUpdate(Context ctx, Kategori obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        for (int i = 0; i < listKategori.size(); i++) {
            if (listKategori.get(i).getIdKategori() != obj.getIdKategori()) {
                if (listKategori.get(i).getKode().equals(obj.getKode())) {
                    MessageUtil.showMessage(ctx, "Kode kategori sudah ada");
                    return false;
                }
            }
        }

        for (int i = 0; i < listKategori.size(); i++) {
            if (listKategori.get(i).getIdKategori() == obj.getIdKategori()) {
                sql.saveUpdateKategori(obj, ctx);
                listKategori.set(i, obj);
                arrayAdapter.notifyDataSetChanged();
                MessageUtil.showMessage(ctx, "Update data berhasil");
                return true;
            }
        }
        return false;
    }

}
