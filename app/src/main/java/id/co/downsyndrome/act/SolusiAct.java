package id.co.downsyndrome.act;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.adapter.SolusiAdapter;
import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.Solusi;
import id.co.downsyndrome.util.MessageUtil;

public class SolusiAct extends Fragment {
    private List<Solusi> listSolusi;
    private SolusiAdapter solusiAdapter;
    private ListView listViewMasterSolusi;
    private RelativeLayout view;

    private boolean isNew;

    public SolusiAct() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(this.getClass().getName(), "ok1");
        view = (RelativeLayout) inflater.inflate(R.layout.mastersolusi_view, container, false);
        getActivity().setTitle("Master Solusi");

        listSolusi = new SqlDownSyndrome().getMasterSolusi(view.getContext());
        solusiAdapter = new SolusiAdapter(view.getContext(), R.layout.mastersolusi_view_list_adapter, listSolusi);

        listViewMasterSolusi = (ListView) view.findViewById(R.id.listMasterSolusiView);

        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.mastersolusi_view_list_header, listViewMasterSolusi, false);
        listViewMasterSolusi.addHeaderView(header);
        listViewMasterSolusi.setAdapter(solusiAdapter);
        listViewMasterSolusi.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
                if (position > 0) {
                    final Solusi obj = (Solusi) parent.getItemAtPosition(position);
                    initContextMenuTable(view, obj);
                }
                return false;
            }
        });


        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.btn_add_mastersolusi);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isNew = true;
                showDialogInput(view.getContext(), new Solusi());
                MessageUtil.showMessage(view.getContext(), "Ok");
            }
        });

        return view;
    }

    private void initContextMenuTable(final View view, final Solusi obj) {
        final CharSequence[] itemMenus = {"Delete", "Edit"};
        final Context ctx = view.getContext();

        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder.setTitle("Action:");
        alertDialogBuilder.setItems(itemMenus, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    MessageUtil.confirmationDelete(ctx,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    ISqlDownSyndrome sql = new SqlDownSyndrome();
                                    sql.deleteMasterSolusi(obj.getIdSolusi(), ctx);
                                    listSolusi.remove(obj);
                                    solusiAdapter.notifyDataSetChanged();
                                    MessageUtil.showMessage(ctx, "Hapus data berhasil");
                                }
                            }
                    );


                } else {
                    isNew = false;
                    showDialogInput(ctx, obj);
                }
            }
        });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void showDialogInput(final Context ctx, final Solusi obj) {
        AlertDialog.Builder alertDialogBuilderEdit = new AlertDialog.Builder(ctx);
        if (isNew) {
            alertDialogBuilderEdit.setTitle("Tambah Master Solusi");
        } else {
            alertDialogBuilderEdit.setTitle("Edit Master Solusi");
        }
        LayoutInflater li = LayoutInflater.from(view.getContext());
        final View masterSolusiInputView = li.inflate(R.layout.master_solusi_input, null);
        alertDialogBuilderEdit.setView(masterSolusiInputView);
        final EditText userInputKet = (EditText) masterSolusiInputView.findViewById(R.id.inputMasterSolusiKet);
        if (!isNew) {
            userInputKet.setText(obj.getKeterangan());
        }

        alertDialogBuilderEdit.setCancelable(false).setPositiveButton("Simpan", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Do nothing here because we override this button later to change the close behaviour.
            }
        });

        alertDialogBuilderEdit.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });


//      alertDialogBuilderEdit.create().show();
        final AlertDialog dialog = alertDialogBuilderEdit.create();
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                obj.setKeterangan(userInputKet.getText().toString().trim());
                if (obj.getKeterangan().equals("")) {
                    MessageUtil.showMessage(ctx, "Keterangan tidak boleh kosong");
                }

                if (isNew) {
                    if (saveNew(ctx, obj)) {
                        dialog.dismiss();
                    }
                } else {
                    if (saveUpdate(ctx, obj)) {
                        dialog.dismiss();
                    }
                }
            }
        });
    }

    private boolean saveNew(Context ctx, Solusi obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        obj.setIdSolusi(sql.saveNewMasterSolusi(obj.getKeterangan(), ctx));
        listSolusi.add(obj);
        solusiAdapter.notifyDataSetChanged();
        MessageUtil.showMessage(ctx, "Tambah data berhasil");
        return true;
    }

    private boolean saveUpdate(Context ctx, Solusi obj) {
        ISqlDownSyndrome sql = new SqlDownSyndrome();
        for (int i = 0; i < listSolusi.size(); i++) {
            if (listSolusi.get(i).getIdSolusi() == obj.getIdSolusi()) {
                sql.saveUpdateMasterSolusi(obj, ctx);
                listSolusi.set(i, obj);
                solusiAdapter.notifyDataSetChanged();
                MessageUtil.showMessage(ctx, "Update data berhasil");
                return true;
            }
        }
        return false;
    }
}
