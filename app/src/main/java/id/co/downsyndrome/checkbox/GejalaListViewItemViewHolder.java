package id.co.downsyndrome.checkbox;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class GejalaListViewItemViewHolder extends RecyclerView.ViewHolder {

    private CheckBox itemCheckbox;
    private TextView textViewKeterangan;
    private TextView textViewKategori;

    public GejalaListViewItemViewHolder(View itemView) {
        super(itemView);
    }

    public CheckBox getItemCheckbox() {
        return itemCheckbox;
    }

    public void setItemCheckbox(CheckBox itemCheckbox) {
        this.itemCheckbox = itemCheckbox;
    }

    public TextView getTextViewKeterangan() {
        return textViewKeterangan;
    }

    public void setTextViewKeterangan(TextView textViewKeterangan) {
        this.textViewKeterangan = textViewKeterangan;
    }

    public TextView getTextViewKategori() {
        return textViewKategori;
    }

    public void setTextViewKategori(TextView textViewKategori) {
        this.textViewKategori = textViewKategori;
    }
}
