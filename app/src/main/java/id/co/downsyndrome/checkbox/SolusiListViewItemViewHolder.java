package id.co.downsyndrome.checkbox;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class SolusiListViewItemViewHolder extends RecyclerView.ViewHolder {

    private CheckBox itemCheckbox;
    private TextView textViewKeterangan;
    private TextView textViewId;

    public SolusiListViewItemViewHolder(View itemView) {
        super(itemView);
    }

    public CheckBox getItemCheckbox() {
        return itemCheckbox;
    }

    public void setItemCheckbox(CheckBox itemCheckbox) {
        this.itemCheckbox = itemCheckbox;
    }

    public TextView getTextViewKeterangan() {
        return textViewKeterangan;
    }

    public void setTextViewKeterangan(TextView textViewKeterangan) {
        this.textViewKeterangan = textViewKeterangan;
    }

    public TextView getTextViewId() {
        return textViewId;
    }

    public void setTextViewId(TextView textViewId) {
        this.textViewId = textViewId;
    }
}
