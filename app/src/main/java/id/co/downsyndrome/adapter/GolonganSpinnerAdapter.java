package id.co.downsyndrome.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import id.co.downsyndrome.model.Golongan;

/**
 * https://stackoverflow.com/questions/1625249/android-how-to-bind-spinner-to-custom-object-list
 * Created by MGS on 10/30/2017.
 */

public class GolonganSpinnerAdapter  extends ArrayAdapter<Golongan> {
    private Context context;
    private Golongan gols[];

    public GolonganSpinnerAdapter(@NonNull Context context, int resource, Golongan gols[]) {
        super(context, resource, gols);
        this.context = context;
        this.gols = gols;
    }

    @Override
    public int getCount(){
        return gols.length;
    }

    @Override
    public Golongan getItem(int position){
        return gols[position];
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(gols[position].getGolongan());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(gols[position].getGolongan());

        return label;
    }

}
