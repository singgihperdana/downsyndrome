package id.co.downsyndrome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.model.Kategori;


/**
 * Created by MGS on 10/17/2017.
 */
public class KategoriAdapter extends ArrayAdapter<Kategori> {

    private LayoutInflater mInflater;
    private List<Kategori> kategoris;
    private int mViewResourceId;

    public KategoriAdapter(Context context, int textViewResourceId,
                           List<Kategori> kategoris) {
        super(context, textViewResourceId, kategoris);

        this.kategoris = kategoris;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
    }

    @Override
    public Kategori getItem(int position) {
        return kategoris.get(position);
    }

    @Override
    public long getItemId(int position) {
        return kategoris.get(position).getIdKategori();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        Kategori obj = kategoris.get(position);

        if (obj != null) {
            TextView tvKode = (TextView) convertView.findViewById(R.id.kode);
            TextView tvKeterangan = (TextView) convertView.findViewById(R.id.keterangan);

            if (tvKode != null) {
                tvKode.setText(obj.getKode());
            }
            if (tvKeterangan != null) {
                tvKeterangan.setText((obj.getKeterangan()));
            }
        }

        return convertView;
    }
}
