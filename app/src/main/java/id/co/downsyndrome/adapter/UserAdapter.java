package id.co.downsyndrome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.model.User;


/**
 * Created by MGS on 10/17/2017.
 */
public class UserAdapter extends ArrayAdapter<User> {

    private LayoutInflater mInflater;
    private List<User> users;
    private int mViewResourceId;

    public UserAdapter(Context context, int textViewResourceId, List<User> users) {
        super(context, textViewResourceId, users);

        this.users = users;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
    }

    @Override
    public User getItem(int position) {
        return users.get(position);
    }

    @Override
    public long getItemId(int position) {
        return users.get(position).getIdUser();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        User obj = users.get(position);

        if (obj != null) {
            TextView tv1 = (TextView) convertView.findViewById(R.id.usernameUser);
            TextView tv2 = (TextView) convertView.findViewById(R.id.namaUser);
            TextView tv3 = (TextView) convertView.findViewById(R.id.pwdUser);

            tv1.setText(obj.getUserName());
            tv2.setText((obj.getNama()));
            tv3.setText("******");
        }

        return convertView;
    }
}
