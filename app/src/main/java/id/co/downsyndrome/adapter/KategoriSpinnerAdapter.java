package id.co.downsyndrome.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import id.co.downsyndrome.model.Kategori;

/**
 * https://stackoverflow.com/questions/1625249/android-how-to-bind-spinner-to-custom-object-list
 * Created by MGS on 10/30/2017.
 */

public class KategoriSpinnerAdapter extends ArrayAdapter<Kategori> {
    private Context context;
    private Kategori vals[];

    public KategoriSpinnerAdapter(@NonNull Context context, int resource, Kategori vals[]) {
        super(context, resource, vals);
        this.context = context;
        this.vals = vals;
    }

    @Override
    public int getCount(){
        return vals.length;
    }

    @Override
    public Kategori getItem(int position){
        return vals[position];
    }

    @Override
    public long getItemId(int position){
        return position;
    }


    // This is for the "passive" state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // I created a dynamic TextView here, but you can reference your own  custom layout for each spinner item
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(vals[position].getKode() + " - " + vals[position].getKeterangan());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }

    // And here is when the "chooser" is popped up
    // Normally is the same view, but you can customize it if you want
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(vals[position].getKode());

        return label;
    }

}
