package id.co.downsyndrome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.model.Solusi;


public class SolusiAdapter extends ArrayAdapter<Solusi> {
    private LayoutInflater mInflater;
    private List<Solusi> solusis;
    private int mViewResourceId;

    public SolusiAdapter(Context context, int textViewResourceId, List<Solusi> solusis) {
        super(context, textViewResourceId, solusis);
        this.solusis = solusis;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
    }

    public Solusi getItem(int position) {
        return solusis.get(position);
    }

    @Override
    public long getItemId(int position) {
        return solusis.get(position).getIdSolusi();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);
        Solusi obj = solusis.get(position);
        if (obj != null) {
            TextView tvKode = (TextView) convertView.findViewById(R.id.idMasterSolusi);
            TextView tvKeterangan = (TextView) convertView.findViewById(R.id.ketMasterSolusi);
            tvKode.setText(Integer.valueOf(obj.getIdSolusi())==null?"null":String.valueOf(obj.getIdSolusi()));
            tvKeterangan.setText(obj.getKeterangan());
        }

        return convertView;
    }
}
