package id.co.downsyndrome.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.checkbox.GejalaListViewItemViewHolder;
import id.co.downsyndrome.model.RuleGejalaSolusi;

public class GejalaCBoxAdapter extends BaseAdapter {

    private List<RuleGejalaSolusi> listGejala = null;

    private Context ctx = null;

    public GejalaCBoxAdapter(Context ctx, List<RuleGejalaSolusi> listGejala) {
        this.ctx = ctx;
        this.listGejala = listGejala;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if (listGejala != null) {
            ret = listGejala.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int itemIndex) {
        Object ret = null;
        if (listGejala != null) {
            ret = listGejala.get(itemIndex);
        }
        return ret;
    }

    @Override
    public long getItemId(int itemIndex) {
        return itemIndex;
    }

    @Override
    public View getView(int itemIndex, View convertView, ViewGroup viewGroup) {

       GejalaListViewItemViewHolder viewHolder = null;

        if (convertView != null) {
            viewHolder = (GejalaListViewItemViewHolder) convertView.getTag();
        } else {
            convertView = View.inflate(ctx, R.layout.gejala_view_list_cbox_adapter, null);
            CheckBox itemCheckbox = (CheckBox) convertView.findViewById(R.id.gejala_checkbox);
            TextView itemKategori = (TextView) convertView.findViewById(R.id.kategori_gejala_cbox);
            TextView itemKeterangan = (TextView) convertView.findViewById(R.id.keterangan_gejala_cbox);
            viewHolder = new GejalaListViewItemViewHolder(convertView);
            viewHolder.setItemCheckbox(itemCheckbox);
            viewHolder.setTextViewKategori(itemKategori);
            viewHolder.setTextViewKeterangan(itemKeterangan);
            convertView.setTag(viewHolder);
        }

        RuleGejalaSolusi obj = listGejala.get(itemIndex);
        viewHolder.getItemCheckbox().setChecked(obj.isChecked());
        viewHolder.getTextViewKategori().setText(""+obj.getKategoriStr());
        viewHolder.getTextViewKeterangan().setText(obj.getKeterangan());

        return convertView;
    }

}
