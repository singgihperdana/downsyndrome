package id.co.downsyndrome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.model.RuleGejalaSolusi;

/**
 * Created by MGS on 10/17/2017.
 */
public class RuleGejalaKatKeterAdapter extends ArrayAdapter<RuleGejalaSolusi> {

    private LayoutInflater mInflater;
    private List<RuleGejalaSolusi> ruleGejalaSolusis;
    private int mViewResourceId;

    public RuleGejalaKatKeterAdapter(Context context, int textViewResourceId,
                                     List<RuleGejalaSolusi> ruleGejalaSolusis) {
        super(context, textViewResourceId, ruleGejalaSolusis);

        this.ruleGejalaSolusis = ruleGejalaSolusis;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
    }

    @Override
    public RuleGejalaSolusi getItem(int position) {
        return ruleGejalaSolusis.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ruleGejalaSolusis.get(position).getId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        RuleGejalaSolusi obj = ruleGejalaSolusis.get(position);

        if (obj != null) {
            TextView tvGol= (TextView) convertView.findViewById(R.id.gejalaGolongan);
            TextView tvKat = (TextView) convertView.findViewById(R.id.gejalaKategori);
            TextView tvKeterangan= (TextView) convertView.findViewById(R.id.gejalaKeterangan);

            if (tvGol!= null) {
                tvGol.setText(obj.getGolonganStr());
            }
            if (tvKat!= null) {
                tvKat.setText(obj.getKategoriStr());
            }
            if (tvKeterangan!= null) {
                tvKeterangan.setText((obj.getKeterangan()));
            }
        }

        return convertView;
    }
}
