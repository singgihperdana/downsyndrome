package id.co.downsyndrome.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.checkbox.SolusiListViewItemViewHolder;
import id.co.downsyndrome.model.Solusi;

public class SolusiCBoxAdapter extends BaseAdapter {

    private List<Solusi> listViewItemSolusi = null;

    private Context ctx = null;

    public SolusiCBoxAdapter(Context ctx, List<Solusi> listViewItemSolusi) {
        this.ctx = ctx;
        this.listViewItemSolusi = listViewItemSolusi;
    }

    @Override
    public int getCount() {
        int ret = 0;
        if (listViewItemSolusi != null) {
            ret = listViewItemSolusi.size();
        }
        return ret;
    }

    @Override
    public Object getItem(int itemIndex) {
        Object ret = null;
        if (listViewItemSolusi != null) {
            ret = listViewItemSolusi.get(itemIndex);
        }
        return ret;
    }

    @Override
    public long getItemId(int itemIndex) {
        return itemIndex;
    }

    @Override
    public View getView(int itemIndex, View convertView, ViewGroup viewGroup) {

        SolusiListViewItemViewHolder viewHolder = null;

        if (convertView != null) {
            viewHolder = (SolusiListViewItemViewHolder) convertView.getTag();
        } else {
            convertView = View.inflate(ctx, R.layout.mastersolusi_view_list_cbox_adapter, null);
            CheckBox itemCheckbox = (CheckBox) convertView.findViewById(R.id.master_solusi_list_view_item_checkbox);
            TextView itemId = (TextView) convertView.findViewById(R.id.master_solusi_list_view_item_id);
            TextView itemKeterangan = (TextView) convertView.findViewById(R.id.master_solusi_list_view_item_keterangan);
            viewHolder = new SolusiListViewItemViewHolder(convertView);
            viewHolder.setItemCheckbox(itemCheckbox);
            viewHolder.setTextViewId(itemId);
            viewHolder.setTextViewKeterangan(itemKeterangan);
            convertView.setTag(viewHolder);
        }

        Solusi obj = listViewItemSolusi.get(itemIndex);
        viewHolder.getItemCheckbox().setChecked(obj.isChecked());
        viewHolder.getTextViewId().setText(""+obj.getIdSolusi());
        viewHolder.getTextViewKeterangan().setText(obj.getKeterangan());

        return convertView;
    }

}
