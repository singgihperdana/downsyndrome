package id.co.downsyndrome.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import id.co.downsyndrome.R;
import id.co.downsyndrome.model.Golongan;


/**
 * Created by MGS on 10/17/2017.
 */
public class GolonganAdapter extends ArrayAdapter<Golongan> {

    private LayoutInflater mInflater;
    private List<Golongan> golongans;
    private int mViewResourceId;

    public GolonganAdapter(Context context, int textViewResourceId, List<Golongan> golongans) {
        super(context, textViewResourceId, golongans);

        this.golongans = golongans;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mViewResourceId = textViewResourceId;
    }

//    @Override
    public Golongan getItem(int position) {
        return golongans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return golongans.get(position).getIdGolongan();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(mViewResourceId, null);

        Golongan obj = golongans.get(position);

        if (obj != null) {
            TextView tv = (TextView) convertView.findViewById(R.id.golongan);
            if (tv != null) {
                tv.setText(obj.getGolongan());
            }
        }

        return convertView;
    }
}
