package id.co.downsyndrome;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import id.co.downsyndrome.db.ISqlDownSyndrome;
import id.co.downsyndrome.db.SqlDownSyndrome;
import id.co.downsyndrome.model.User;
import id.co.downsyndrome.util.AppConstants;
import id.co.downsyndrome.util.AppUtil;
import id.co.downsyndrome.util.MessageUtil;
import id.co.downsyndrome.util.SessionUtil;

/**
 * A login screen that offers login via email/password.
 */
public class ActLogin extends Activity {
    private Button b1, b2;
    private EditText ed1, ed2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_login);

        b1 = (Button) findViewById(R.id.button);
        ed1 = (EditText) findViewById(R.id.editText);
        ed2 = (EditText) findViewById(R.id.editText2);

        b2 = (Button) findViewById(R.id.button2);


        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (ed1.getText().toString().equals("admin") && ed2.getText().toString().equals("admin")) {
                ISqlDownSyndrome sql = new SqlDownSyndrome();
                if(ed1.getText().toString().equals("")){
                    MessageUtil.showMessage(getApplicationContext(), "Username wajib diisi");
                    return;
                }else if(ed2.getText().toString().equals("")){
                    MessageUtil.showMessage(getApplicationContext(), "Password wajib diisi");
                    return;
                }

                User usr = sql.login(v.getContext(), ed1.getText().toString(), ed2.getText().toString());
                if (usr != null) {
                    SessionUtil.createSession(getApplicationContext(), usr);
                    MessageUtil.showMessage(getApplicationContext(), "Login berhasil. Redirecting...");
                    Intent mainPage = new Intent(ActLogin.this, ActMain.class);
                    startActivity(mainPage);
                } else {
                    MessageUtil.showMessage(getApplicationContext(), "Gagal login");
                }
            }
        });

//        b2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                showDialogExit();
//            }
//        });

        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainPage = new Intent(ActLogin.this, ActMain.class);
                startActivity(mainPage);
            }
        });
    }

    @Override
    public void onBackPressed() {
        showDialogExit();
    }

    private void showDialogExit() {
        new AlertDialog.Builder(ActLogin.this)
                .setTitle("Konfimasi")
                .setMessage("Apakah anda yakin akan keluar?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.finishAffinity(ActLogin.this);
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

}

